package main

/* Aufgaben:
 *
 * 1. Implementieren Sie die Funktion isbn13Checksum() in der Datei isbnchecksum.go.
 *    - Beachten Sie den Test in isbnchecksum_test.go
 * 2. Schreiben Sie hier ein Programm, das den Benutzer nach einer Eingabe von 13
 *    Ziffern fragt und das prüft, ob die eingegebenen Ziffern eine gültige
 *    ISBN-13-Nummer inkl. Prüfziffer sind.
 *
 * Achten Sie bei Ihrem Programm auf eine sinnvolle Struktur:
 * - Welche Hilfsfunktionen könnten sinnvoll sein, um die Prüfung von Eingabe und
 *   ISBN-Nummer möglichst übersichtlich zu schreiben?
 */

func main() {

}

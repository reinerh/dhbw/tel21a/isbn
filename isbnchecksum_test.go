package main

import "fmt"

func Example_isbn13Checksum() {
	l1 := []int{9, 7, 8, 3, 4, 5, 5, 5, 0, 2, 3, 6}
	fmt.Println(isbn13Checksum(l1))

	// Output:
	// 7
}

package main

import "gitlab.com/reinerh/dhbw/tel21a/listmath"

// Erwartet eine Liste von Ziffern und berechnet deren ISBN-13-Prüfsumme.
func isbn13Checksum(digits []int) int {
	// TODO
	// Vervollständigen Sie hier den Code.
	// Die Funktion listmath.Sum() kann/sollte verwendet werden, allerdings nicht auf
	// diese einfache Weise. Sie ist hier zu Demonstrationszwecken schon eingebaut.
	return listmath.Sum(digits)
}

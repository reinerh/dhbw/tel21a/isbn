# Programm zur Bestimmung von ISBN-Prüfziffern.

## Aufgaben:
 
1. Implementieren Sie die Funktion isbn13Checksum() in der Datei isbnchecksum.go.
   - Beachten Sie den Test in isbnchecksum_test.go
2. Schreiben Sie hier ein Programm, das den Benutzer nach einer Eingabe von 13
   Ziffern fragt und das prüft, ob die eingegebenen Ziffern eine gültige
   ISBN-13-Nummer inkl. Prüfziffer sind.
 
Achten Sie bei Ihrem Programm auf eine sinnvolle Struktur:
- Welche Hilfsfunktionen könnten sinnvoll sein, um die Prüfung von Eingabe und
   ISBN-Nummer möglichst übersichtlich zu schreiben?

## Hinweise:

* Das Package `listmath` wird mit der folgenden Zeile in eine Go-Datei eingebunden:
```
import "gitlab.com/reinerh/dhbw/tel21a/listmath"
```
* Der Import wird auf der Konsole folgendermaßen durchgeführt:
```
go get gitlab.com/reinerh/dhbw/tel21a/listmath
```
* Damit der Befehl `go get` funktioniert, muss das Programm [Git](https://git-scm.com/) installiert bzw. im Pfad sein. In der für die Vorlesung bereitgestellten Umgebung ist dies der Fall, wer ein eigenes VSCode benutzt, sollte Git selbst installieren.
